# Change Log

## [2023.3.0] - 2023-06-22

### Added
- new dependency: [damusic](https://gitlab.com/softapricot/django-apricot-music)

### Changed
- moved music package and relatives to its own repo

## [2023.2.3] - 2023-06-22

### Changed
- updated dependencies

## [2023.2.2] - 2023-06-19

### Added
- Single use integration compatibility

### Added
- Single use integration compatibility

### Fixed
- Templates style missmatch

## [2023.2.1] - 2023-06-15

### Fixed
- models_module wrong setting

## [2023.2.0] - 2023-06-15

### Added
- new dependency: [dafitness](https://gitlab.com/softapricot/django-apricot-fitness)

### Changed
- moved fitness package and relatives to its own repo

## [2023.2.0a1] - 2023-06-12

### Added
- `total_stats` dafitness template tag with customizable time range

## [2023.2.0a0] - 2023-06-10

### Added
- new dependecy: [dacommon](https://gitlab.com/softapricot/django-apricot-common)

### Changed
- moved common package and relatives to its own repo

### Fixed
- dafitness tag error on non authenticated user

## 2023.1.4

### Added
- (Music) 'spotify_top_songs' and 'spotify_top_artists' template tags
- (Common) integrations cache capability
- (Fitness) strava 'activities' template tag
- sphinx autodoc and autodeploy to gitlab pages

## 2023.1.2

### Changed
- extended IntegrationToken.token max_length

## 2023.1.1

### Changed
- version fixed

## 2023.1.0

### Added
- (Music) base spotify integration
- (Music) currently_playing tag
- (Music) integration status badge tag

### Changed
- General scss fixes and adjustments
- (Accounts) views fixes and style integration
- (Fitness) fixed strava token refreshing
- fixed integration token management
- unique tokens per user per integration management

## 2023.0.4

### Added
- (Fitness) app with Strava integration.
- (Accounts) package as a django's reusable app
- account's requests to admin with approved link
- configurable 'username field' to choose 'username' or 'email' for authentication

### Changed
- 'core' package renamed as 'common'
- account's request mode fixes

## 2023.0.3

### Added
- changelog file

### Changed
- CI pipelines moved to Jenkins. CD stays on gitlab-ci
- Requeriments management moved to Poetry
